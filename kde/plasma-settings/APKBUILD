# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-settings
pkgver=0_git20190930
pkgrel=0
_commit="5a590eb6a2a0a666f4d0910bd6dcafec19b5253f"
pkgdesc="Settings application for Plasma Mobile"
arch="all"
url="https://community.kde.org/Plasma/Mobile"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="kirigami2 qt5-qtquickcontrols2 kded"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev kwindowsystem-dev kcoreaddons-dev solid-dev kconfig-dev kauth-dev kdbusaddons-dev ki18n-dev kdeclarative-dev kio-dev kdelibs4support-dev karchive-dev kservice-dev kpackage-dev kconfigwidgets-dev kbookmarks-dev kcrash-dev kcompletion-dev kdesignerplugin-dev kdesignerplugin kjobwidgets-dev kdoctools-dev kemoticons-dev kguiaddons-dev kitemmodels-dev kinit-dev knotifications-dev kparts-dev kunitconversion-dev plasma-framework-dev"
source="$pkgname-$_commit.tar.gz::https://invent.kde.org/kde/plasma-settings/-/archive/$_commit/plasma-settings-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	mkdir "$builddir"/build
}

build() {
	cd "$builddir"/build
	cmake "$builddir" \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=true ctest
}

package() {
	cd "$builddir"/build
	DESTDIR="$pkgdir" make install
}
sha512sums="6264c0d92f2a6aad6da29ba4ba56b0c75cb73a5ada6dc054b307a371ebe8183c2786319d88332a109d39eaee2574f7845c64c88bd9603576fc2e6e6f8e6cb6a3  plasma-settings-5a590eb6a2a0a666f4d0910bd6dcafec19b5253f.tar.gz"
